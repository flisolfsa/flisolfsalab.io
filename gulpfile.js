
const autoPrefixBrowserList = ["last 2 version", "safari 5", "ie 8", "ie 9", "opera 12.1", "ios 6", "android 4"];

const gulp          = require("gulp");
const gutil         = require("gulp-util");
const bs            = require("browser-sync");
const plumber       = require("gulp-plumber");
const sourceMaps    = require("gulp-sourcemaps");
const sass          = require("gulp-sass");
const concat        = require("gulp-concat");
const autoprefixer  = require("gulp-autoprefixer");
const gulpSequence  = require("gulp-sequence");
const shell         = require("gulp-shell");
const uglify        = require("gulp-uglify");
const cssmin        = require("gulp-cssmin");
const usemin        = require("gulp-usemin");
const imagemin      = require("gulp-imagemin");
const babel         = require("gulp-babel");

gulp.task("browserSync", function() {
    bs({
        server: {
            baseDir: "dev/"
        },
        options: {
            reloadDelay: 250
        },
        notify: false
    });
});

gulp.task("js", function() {
    return gulp.src("dev/js/**/*.js")
            .pipe(plumber())
            .pipe(bs.reload({stream: true}))
            .on("error", gutil.log);
});

gulp.task("css", function() {
    return gulp.src("dev/styles/scss/init.scss")
            .pipe(plumber({
                errorHandler: function (err) {
                    console.log(err);
                    this.emit("end");
                }
            }))
            .pipe(sourceMaps.init())
            .pipe(sass({
                errLogToConsole: true,
                includePaths: [
                    "dev/styles/scss/"
                ]
            }))
            .pipe(autoprefixer({
                browsers: autoPrefixBrowserList,
                cascade:  true
            }))
            .on("error", gutil.log)
            .pipe(concat("styles.css"))
            .pipe(sourceMaps.write())
            .pipe(gulp.dest("dev/styles"))
            .pipe(bs.reload({stream: true}));
});

gulp.task("html", function() {
    return gulp.src("dev/*.html")
            .pipe(plumber())
            .pipe(bs.reload({stream: true}))
            .on("error", gutil.log);
});

gulp.task("html-deploy", function() {
    return gulp.src("dev/*.html")
        .pipe(plumber())
        .pipe(usemin({
            "js": [ babel({presets: ['@babel/env']}), uglify ],
            "css": [ cssmin ]
        }))
        .pipe(gulp.dest("./public"));
});

gulp.task("images", function() {
    return gulp.src("./dev/images/**/*")
        .pipe(plumber())
        .pipe(imagemin())
        .pipe(gulp.dest("./public/images"));
});

gulp.task("copy", function() {
    gulp.src(["dev/*", "!dev/*.html"])
        .pipe(plumber())
        .pipe(gulp.dest("public"));

    //grab any hidden files too
    gulp.src("dev/.*")
        .pipe(plumber())
        .pipe(gulp.dest("public"));

    gulp.src("dev/fonts/**/*")
        .pipe(plumber())
        .pipe(gulp.dest("public/fonts"));
});

gulp.task("clean", function() {
    return shell.task(["rm -rf public"]);
});

gulp.task("scaffold", function() {
    return shell.task([
        "mkdir public",
        "mkdir public/fonts",
        "mkdir public/images",
        "mkdir public/scripts",
        "mkdir public/styles"
    ]);
});

gulp.task("default", ["browserSync", "css"], function() {
    gulp.watch("dev/styles/scss/**", ["css"]);
    gulp.watch("dev/js/**", ["js"])
    gulp.watch("dev/*.html", ["html"]);
});

gulp.task("deploy", gulpSequence("clean", "scaffold", "copy", ["html-deploy", "images"]));
