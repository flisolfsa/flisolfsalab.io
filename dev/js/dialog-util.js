NodeList.prototype.addItemEventListener = function(event, callback) {
    console.log(this);
    this.forEach(el => el.addEventListener(event, callback, false));
};

document.querySelectorAll('*[data-dialog]')
        .addItemEventListener('click', function() {
            let dialogId = this.getAttribute('data-dialog');
            let selectedModal = document.querySelector('#' + dialogId);
            selectedModal.style.display = 'block';
            document.body.style.overflow = 'hidden';

            selectedModal.addEventListener('click', evt => {
                if(evt.target.className == 'modal-close' ||
                   evt.target.className == 'modal' ){
                    selectedModal.style.display = 'none';
                    document.body.style.overflow = 'auto';
                }
            }, false);
            
        });
